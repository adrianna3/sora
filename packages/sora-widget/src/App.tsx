import { Button } from 'sora-ui';

function App({domElement}: {domElement: HTMLElement | null}) {
  console.log('domElement', domElement)
  return (
    <div className="App">
      <header className="App-header">
        <Button text='Heey' onClick={() => {
          console.log('Heey after build')
        }}/>
      </header>
    </div>
  );
}

export default App;
